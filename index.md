# Index

1. Recruiting
   1. [Market](recruiting/market.md)
   2. [Sourcing](./recruiting/sourcing.md)
   3. [Operations](recruiting/operations.md)
   4. [Team](recruiting/team.md)
   5. [Interviews](recruiting/interviews.md)
2. Org design
   1. [Product Org](org/product-org.md)
3. People 
   1. Goal setting
   2. Performance
   3. Incentives
