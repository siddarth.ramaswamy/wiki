# [Draft] Product Org

[Home](../index.md)

YMMV. Caveat emptor.

What is a product org? A mental model I favour is that it is a system that accepts capital, skilled executors, management input and market information and uses these to create value by putting software in the hands of customers.

###  Key considerations

1. Budget (How much can we pay?)
2. Acquisition (How do we find and hire people?)
3. Skills (What skills do we need, and how do we get them?)
4. Processes (What workflows that will be followed by the people we hire in order to create value for the customer? How do we _do_ work, in essence?)

### Broad problem areas

Value for end users and customers almost always translates into one or more of status, information, money, convenience and time. A software product company aims to use software as a key means of delivering this value.

Based on this mental model of value, one way to organize a product org is on the basis of the customer being served, and the nature of the value created for them.

3 common groups that emerge:

1. End-user teams
2. Platform teams
3. Skill teams (or skill functions)

It's important to note that in early stage businesses, _the same team may cover all three bases_. Specialization becomes necessary as the business scales.

1. **End-user teams** - Teams that create value for (internal or external) end users by shipping products
   1. Usually focused on improving end-user driven metrics like revenue, successful txns
   2. End users could be external consumers or interal ones like operations, sales, marketing, HR etc.
      1. Teams that serve internal end users have strong analogues to B2B saas businesses
   3. Need to iterate faster than the competition
   4. Usually led by a product manager and staffed with a mix of folks with appropriate skills (aka cross-functional team aka squad aka pod aka 2-pizza team)
2. **Platform teams** - Teams that create value for the product org itself by shipping products
   1. Centralize ownership of problems shared by several end-user teams
      1. Consequently also accountable for establishing and driving standardization
   2. Usually focused on improving productivity metrics (eg. rate of deployments) and quality metrics (eg. uptime, adoption of a new standard)
   3. Same as end-user teams - PM + x-fn squad, though often require higher technical focus
   4. Have strong analogues to B2B SaaS businesses
      1. Helpful to think of them as a B2B SaaS vendor that's been acquired to focus exclusively on the parent
      2. End-user teams acquire tooling/infra/etc budgets that they spend with platform teams.
3. **Skill teams** (aka functions) - Teams that create value for end-user and platform teams by providing skills as a service
   1. Focused on providing necessary skills to end-user and platform teams through 
      1. hiring
      2. training
      3. feedback
      4. incentives
      5. pricing
      6. best practices
   2. Skill teams have strong analogues to consulting firms
   3. End-user and Platform teams acquire staffing budgets, and spend those budgets with skill teams to get their projects staffed
   4. Do not own business goals (though would have critical input to both consumer and platform teams in deciding them)
   5. Have to work with other skill functions to figure out and implement the answer to "How do we best build software?"

### Risks and problem areas

The two most frequent risks with this setup is not-enough-skin-in-the-game/solipsism and us-vs-them.

#### End-user teams

They are closest to end users. End-user teams tend to focus on direct impact to the user, and are less liable to think about improving overall productivity, quality and impact of the wider org. 

In situations of rapid growth, their targets and incentives push them toward short term thinking and piling on technical debt in the process of moving numbers. 

##### Relationships

They will tend to feel that platform teams don't care enough about the customer or are outright wasting time building shitty products that they are forced to use. 

They will tend to feel that skill teams are either too academic/not hands-on, especially in the matter of performance evaluation. They will also almost always feel that skill functions are failing to supply them with adequate numbers of people.

#### Platform teams

They are one step removed from end users. Platform teams are liable to focus on long term efficiency and productivity while losing focus on benefitting the end user _today_. They also risk low accountability because their impact is harder to quantify unless focussed on. This lack of direct feedback loops means they could become increasingly solipsistic, pursuing goals that do not actually benefit the wider business.

Failures on a platform team will usually hurt multiple consumer teams and their work.

##### Relationships

They will tend to feel that consumer teams are selfish and don't think of the greater good. They will also tend to believe that they possess superior technical skills. If the team is successful, then this may even be true, in which case they can cause a brain drain from other teams.

Platform teams will almost always have key segments in the company that question their value and impact. 

#### Skill teams

Of the three, they are furthest removed from the end user, indeed, from the core business, and consequntly have the least skin in the game. The most common issue with skill teams is that they tend to not formally exist at all, or be delegated to HR. 

Because of their distance from and lack of accountability for core business metrics, skill teams run the highest risk of solipsism, churning out reams of documentation and best practices that the other two groups don't care about and don't adopt. 

Another risk is that they fail to balance quantity and quality of of the folks they provide to the other group and end up either severely shorthanded, or severely underskilled.

Finally folks on skill teams may suffer from poor morale after a while because most of their work is administration, rather than the exercise of the skill which got them into this position and role.

##### Relationships

They will tend to feel that everyone else lacks process, and do not follow best practices.

#### Mitigation

1. us-vs-them and no-skin-in-the-game/solipsism
   1. lots of face time between teams that need to collaborate
   2. regular staffing rotations across end-user, platform and skill so that folks build up a better empathy for all three points of view
   3. all skill team members also work end-user or platform pieces (in non-critical roles and/or part time) so that they remain hands-on
2. Early warning
   1. Unlimited sick leave
      1. fantastic early warning mechanism - teams that are starting to go south will inevitably record a spike in sick leaves weeks or months in advace of any externally visible symptoms
   2. Lightly regulated internal tranfers
      1. the teams everyone wants to leave and nobody wants to transfer to are the ones with problems

