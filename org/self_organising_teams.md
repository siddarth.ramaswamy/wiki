The pipe dream for any organization is to build self organising teams, where they are given the batton of the product and the team runs with it solving the most burning issue for the product at any given point in time. This is easy to talk about, but one of the hardest problems to solve and scale.

Why is it a hard problem to solve:
- Lack of investment in building sensors for the team to track their velocity, velocity sadly is not what happens on a project board. Its what happens on and off the board
- Inefficient or no feedback loops from the time dev is done to when it hits production
- Concentration is more on how much planning is done, rather than how much work is done
- We seldom fix the environment, rather we try to fix the individual
- Focus is on process / output, rather than the expected outcome

Measure true velocity
- Build sensors in the team to understand pepole and tech related issues
- You don't need a tool to solve this problem, google sheets will do
- Build a feedback loop to understand what sensors you need to setup
- Follow the collect, measure, observe, iterate cycle to make sense of your sensors
- Eg: Meetings Attended, Sick Leave, Support Tasks, Functional Tasks

Improve Feedback Loops
- No task croses an iteration, if you cannot break it down :shurg:
- Release to production after every iteration
- Practice writing tests (its ok if you don't do TDD, atleast write tests)
- Have acceptance feedback loops - QA & Customer

Fix the environment, not the people
- Hire the best workfoce you can "afford" and empower them to solve the problems
- Micro Management is an end result of you not having the right sensors, make the data visible
- Not able to iterate quick enough, is due to lack of Continous Delivery. Invest in it
- If you have managers and they aren't having a regular cadence fire them
- Don't implement OKRs, you don't need them. You need a way to get your teams to commit to a goal and achieve it.
- Empower the execution team (who are on the floor) to define the goals
- Treat everyone in your organisation as a customer

Be outcome oriented
Read [Drive](https://www.goodreads.com/book/show/6452796-drive) and watch [this](https://www.youtube.com/watch?v=OqmdLcyES_Q). The moment irrespective of where you are in the org, you try to focus on the process, you would always focus on data points to improve the process. You can keep tweaking a process without seeing an outcome, every step you take forward would derail you 10 steps back.

Summary
To truly create self-organising teams, you need:
- Hire the right people
- Trust them to do the job
- Fix the environment and empower them
- Monitor the outcomes, and give feedback
